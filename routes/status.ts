/**
 * Created by nicholas on 3/2/2017.
 */

import express = require('express');
import bodyparser = require('body-parser');
import {logger} from "../scripts/logger";

const app = express();

app.use(bodyparser.json());

const router = express.Router();
const log = logger(__filename);

/* Main entry point to the bot. */
router.get('/status', function (req, res) {

    const funcName = 'router.post';

    try {

        if (req.body.inline_query) {
            return;
        }

        log.debug(funcName, JSON.stringify(req.body, null, 2));

    }
    catch (e) {
        log.debug(funcName, e);
    }
    finally {
        res.status(200);
        res.send("<html><body>I am working</body></html>");
    }
});

export = router;
