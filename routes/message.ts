/**
 * Created by nicholas on 3/2/2017.
 */

import express = require('express');
import bodyparser = require('body-parser');
import {sendTelegramResponse, TelegramRequest, TelegramResponse} from "../scripts/telegram";
import {handlePythonRequest} from "../scripts/pythonexecutor";
import {handleJavaRequest} from "../scripts/javaexecutor";
import {getRedditPost} from "../scripts/reddit";
import {quoteMe, setQuote} from "../scripts/quoter";
import {handleStockRequest, handleStocktwits} from "../scripts/stock";
import {handleMessage} from "../scripts/messagemanager";
import {marketFinviz, trendingFinviz} from "../scripts/finviz";
import {logger} from "../scripts/logger";
import {handleJavascriptRequest} from "../scripts/javascriptexecutor";
import {executeShell} from "../scripts/shellExecutor";
import {getPrice} from "../scripts/cryptoprice";

const app = express();

app.use(bodyparser.json());

const router = express.Router();
const log = logger(__filename);

/* Main entry point to the bot. */
router.post('/', function (req, res) {

    const funcName = 'router.post';

    try {

        if (req.body.inline_query) {
            handleInlineQuery(req.body);
            return;
        }

        log.debug(funcName, JSON.stringify(req.body, null, 2));

        const telegramRequest = <TelegramRequest>(req.body.message);
        const requestText = telegramRequest.text.toLowerCase().trim();

        const func = commands[requestText];

        if (func) {
            func(telegramRequest);
        }
        else if (cryptoCoin.indexOf(requestText) !== -1) {
            getPrice(telegramRequest);
        }
        else {

            handleMessage(telegramRequest);

        }

        // if (requestText.indexOf('/stocktwits') === 0) {
        //
        //     log.debug(funcName, 'got an stocktwits request');
        //     handleStocktwits(telegramRequest);
        // }
        // else if (requestText.indexOf('/javascript') === 0) {
        //
        //     log.debug(funcName, 'got a javascript request');
        //     handleJavascriptRequest(telegramRequest);
        // }
        // else if (requestText.indexOf('/python') === 0) {
        //
        //     log.debug(funcName, 'got a python request');
        //     handlePythonRequest(telegramRequest);
        // }
        // else if (requestText.indexOf('/java') === 0) {
        //
        //     log.debug(funcName, 'got a java request');
        //     handleJavaRequest(telegramRequest);
        // }
        // else if (requestText.indexOf('/shell') === 0) {
        //
        //     log.debug(funcName, 'got a shell request');
        //     executeShell(telegramRequest);
        // }
        // else if (requestText.indexOf('/setquote') === 0) {
        //
        //     log.debug(funcName, 'got a setquote request');
        //     setQuote(telegramRequest);
        // }
        // else if (requestText.indexOf('/quoteme') === 0) {
        //
        //     log.debug(funcName, 'got a quoteme request');
        //     quoteMe(telegramRequest);
        // }
        // else if (requestText.search(/^\/r .*/) === 0) {
        //
        //     log.debug(funcName, 'got a subreddit request');
        //     getRedditPost(telegramRequest);
        // }
        // else if (requestText.search(/\/stock/) === 0) {
        //
        //     log.debug(funcName, 'got a request for stock data');
        //     handleStockRequest(telegramRequest);
        // }
        // else if (requestText.indexOf('/commands') === 0) {
        //
        //     log.debug(funcName, 'got a request to list commands');
        //     listCommands(telegramRequest);
        // }
        // else if (requestText.indexOf('/finviz market') === 0) {
        //
        //     log.debug(funcName, 'got a request for finviz market');
        //     marketFinviz(telegramRequest);
        // }
        // else if (requestText.indexOf('/finviz') === 0) {
        //
        //     log.debug(funcName, 'got a request for finviz');
        //     trendingFinviz(telegramRequest);
        // }
        // else if (['/btc', '/doge', '/bch', '/ltc'].find(it => it === requestText.trim())) {
        //     log.debug(funcName, 'got a request for cryptoprice');
        //     getPrice(telegramRequest);
        // }
        // else {
        //
        //     log.debug(funcName, 'regular message received');
        //     handleMessage(telegramRequest);
        // }
    }
    catch (e) {
        log.error(funcName, e);
    }
    finally {

        res.status(200);
        res.send();
    }
});

function handleInlineQuery(input: JSON) {

    log.debug('handleInlineQuery', `${JSON.stringify(input, null, 2)}`);
}

function listCommands(telegramrequest: TelegramRequest) {

    const commands = [
        '/r <subreddit>',
        '/stocktwits [sort] [column]',
        '/python <code>',
        '/java <code>',
        '/javascript <code>',
        '/setquote <quote> - <name>',
        '/quoteme <name>',
        '/stock <ticker>',
        '/finviz'];

    const formatted = commands.join('\n');
    sendTelegramResponse(new TelegramResponse(formatted, `${telegramrequest.chat.id}`));
}

const commands = {
    '/commands': listCommands,
    '/stocktwits': handleStocktwits,
    '/javascript': handleJavascriptRequest,
    '/python': handlePythonRequest,
    '/java': handleJavaRequest,
    '/shell': executeShell,
    '/r': getRedditPost,
    '/stock': handleStockRequest,
    // todo add stuff for finviz and crypto
};

const cryptoCoin = [
    '/btc',
    '/bch',
    '/eth',
    '/etc',
    '/doge',
    '/ltc'
];

export = router;
