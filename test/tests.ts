import cheerio = require("cheerio");
import getGoogleStock = require('../scripts/googlestock');
import {fetchAndParseGoogleStock, fetchStockNews} from "../scripts/googlestock";
import {error, log} from "util";
import {getMarketGraphs} from "../scripts/finviz";

/**
 * Created by nicholas on 3/3/17.
 */

getMarketGraphs((urls) => console.log(urls));
