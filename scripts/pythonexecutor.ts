import {executeCommand} from "./commandExecutor";
import {sendTelegramResponse, TelegramRequest, TelegramResponse} from "./telegram";
import {log} from "util";

/**
 * Created by nicholas on 4/26/17.
 */

export function handlePythonRequest(request: TelegramRequest) {

    const pythonCommand = request.text.substr(7).trim();
    const executed = executePython(pythonCommand);

    executed.then(result => {
        sendTelegramResponse(new TelegramResponse(result, `${request.chat.id}`));
    });
    executed.catch(error => {
        log(`handlePythonRequest() ${error}`);
        sendTelegramResponse(new TelegramResponse(`${error}`, `${request.chat.id}`));
    });
}

function executePython(command: string): Promise<string> {

    return new Promise((resolve, reject) => {

        if (command.indexOf("open(") != -1) {
            resolve('nuh uh. you ain\'t gettin no file access!');
        }
        else {
            const python = executeCommand('python', ['-c', command]);
            python.then(result => resolve(result));
            python.catch(err => reject(err));
        }
    });
}
