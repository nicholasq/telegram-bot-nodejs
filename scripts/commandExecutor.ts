/**
 * Created by nicholas on 4/26/17.
 */
import {Logger} from "./logger";

/**
 * Created by nicholas on 4/26/17.
 */

const log = new Logger(__filename);

export function executeCommand(command: string, args: string[]): Promise<string> {
    return new Promise<string>((resolve, reject) => {

        try {

            const spawn = require('child_process').spawn;
            const executed = spawn(command, args);
            let result = '';

            executed.stdout.on('data', (data) => {
                result += `${data}`;
            });
            executed.stderr.on('data', (data) => {
                result += `${data}`;
            });
            executed.on('close', (code) => {

                if (code !== 0) {
                    reject(result);
                }
                else {
                    resolve(result);
                }
            });

            setTimeout(() => {
                executed.kill('SIGINT');
            }, 10000);

        } catch (err) {

            log.error('executeCommand', 'caught error in executeCommand');

            log.error('executeCommand', `${err}`);

            reject(`${err}`);
        }
    });
}
