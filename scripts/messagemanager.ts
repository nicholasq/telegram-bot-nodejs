import {deleteMessage, sendTelegramResponse, TelegramRequest, TelegramResponse} from "./telegram";
import {Logger} from "./logger";
import {handleCleverBot} from "./cleverbot";

const log = new Logger(__filename);
const pwned = {};
const pwnMsgs = {};
const picPwned = {};

let cleverbotRecentlyCalled = {name: '', called: false, responses: 0};
let lastDanielVidPost = Math.floor(Date.now() / 1000) * 60 * 60;

export function handleMessage(telegramRequest: TelegramRequest) {

    const questionForCleverBot = telegramRequest.text.search(/.*[jJ]arvis.*\??/);

    if (telegramRequest.chat.type === 'private') {
        handleCleverBot(telegramRequest);
        return;
    }
    else if (questionForCleverBot > -1) {

        log.debug('handleMessage', `got a question for cleverbot`);

        cleverbotRecentlyCalled.name = telegramRequest.from.first_name.toLowerCase();
        cleverbotRecentlyCalled.called = true;
        handleCleverBot(telegramRequest);
        return;
    }
    else if (cleverbotRecentlyCalled.name == telegramRequest.from.first_name.toLowerCase() &&

        cleverbotRecentlyCalled.called &&
        cleverbotRecentlyCalled.responses < 5) {

        log.debug('handleMessage', 'cleverbot recently called');

        handleCleverBot(telegramRequest);
        cleverbotRecentlyCalled.responses++;
        return;
    }
    else {
        cleverbotRecentlyCalled.responses = 0;
        cleverbotRecentlyCalled.called = false;
        cleverbotRecentlyCalled.name = '';
    }

    log.debug('handleMessage', `${JSON.stringify(telegramRequest, null, 2)}`);

    if (telegramRequest.from.first_name.trim() == 'Nicholas') return handleNick(telegramRequest);
    if (telegramRequest.from.first_name.trim() == 'Daniel') return handleDaniel(telegramRequest);

    handlePwns(telegramRequest);
    handlePicPwns(telegramRequest);

}

function handleDaniel(telegramRequest: TelegramRequest) {

    log.debug('handleDaniel', 'got a message from Daniel');

    const now = Math.floor(Date.now() / 1000) * 60 * 60;
    const message = telegramRequest.text.toLowerCase();

    if (now - lastDanielVidPost < 1) {

        if (message.search('http') != -1) {

            log.debug('handleDaniel', 'deleting daniel\'s youtube');

            deleteMessage(telegramRequest);
            sendTelegramResponse(new TelegramResponse('Sorry. One vid per hour. Make it count!', telegramRequest.chat.id));
        }

    }
    else {
        lastDanielVidPost = now;
    }

}

function handlePwns(telegramRequest: TelegramRequest) {

    const firstname = telegramRequest.from.first_name.toLowerCase();

    if (pwned[firstname]) {

        log.debug('handlePwns', `pwning ${firstname}`);

        deleteMessage(telegramRequest);

        if (pwnMsgs[firstname]) {

            const reason = pwnMsgs[firstname];
            sendTelegramResponse(new TelegramResponse(`You have been pwned for: \n${reason}`, `${telegramRequest.chat.id}`));
        }
    }
}

function handleNick(telegramRequest: TelegramRequest) {

    log.debug('handleNick', 'got a message from Nick');

    const commands = telegramRequest.text.toLowerCase().replace(/\s+/g, ' ').trim().split(' ');

    if (commands.length < 2) return;

    if (commands[0] === 'pwn') {

        log.debug('handleNick', `pwning ${commands[1]}`);

        pwned[commands[1]] = true;

        if (commands.length > 2) {

            const reason = commands.slice(2).join(' ');

            log.debug('handleNick', `pwning for reason: ${reason}`);

            pwnMsgs[commands[1]] = reason;
        }
    }
    else if (commands[0] === 'unpwn') {

        pwned[commands[1]] = false;
    }
    else if (commands[0] === 'picpwn') {

        const firstName = commands[1].toLowerCase();

        log.debug('handleNick', `pic pwning ${firstName}`);

        picPwned[firstName] = true;
    }
    else if (commands[0] === 'unpicpwn') {

        const firstName = commands[1].toLowerCase();

        log.debug('handleNick', `pic unpwning ${firstName}`);

        picPwned[firstName] = false;
    }
}

function handlePicPwns(telegramRequest: TelegramRequest) {

    const firstName = telegramRequest.from.first_name.toLowerCase();

    if (picPwned[firstName]) {

        log.debug('handlePicPwns', `${firstName} should have pic pwned`);

        if (telegramRequest.photo != null ||
            telegramRequest.document != null) {

            log.debug('handlePicPwns', `deleting ${firstName} pic post`);

            deleteMessage(telegramRequest);
        }
        else {
            log.debug('handlePicPwns', `${firstName} - did NOT have pic pwned`);
        }
    }
}
