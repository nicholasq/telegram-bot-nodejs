import https = require('https');
import {sendTelegramResponse, TelegramRequest, TelegramResponse} from "./telegram";
import {Logger} from './logger';

/**
 * Created by nicholas on 5/17/17.
 */

let lastRequest = new Date();
let postCount = 0;
const log = new Logger(__filename);

export function getRedditPost(telegramRequest: TelegramRequest) {

    const now = new Date();

    if (telegramRequest.from.first_name.toLowerCase() !== 'nicholas' &&

        now.getHours() === lastRequest.getHours() &&
        now.getMinutes() === lastRequest.getMinutes() &&
        postCount < 4) {

        log.debug('getRedditPost', `got reddit request too soon`);

        sendTelegramResponse(new TelegramResponse(`no.`, telegramRequest.chat.id));
        return;
    }
    else {

        log.debug('getRedditPost', 'got reddit request');

        lastRequest = now;

        if (postCount++ === 4) postCount = 0;
    }

    log.debug('getRedditPost', 'last request made: ${lastRequest}');

    const params = telegramRequest.text.toLowerCase().replace(/\s+/g, ' ').split(' ');
    const subreddit = params[1];
    let redditUrl = `https://www.reddit.com/r/${subreddit}`;
    let filter = '';

    if (params.length > 2) {

        if (params[2] === 'new') filter = 'new';
        else if (params[2] === 'rising') filter = 'rising';
        else if (params[2] === 'controversial') filter = 'controversial';
        else if (params[2] === 'top') filter = 'top';
        else filter = 'hot';
    }
    else {
        filter = 'hot';
    }

    log.debug('getRedditPost', `subreddit requested: ${subreddit}, filter: ${filter}`);

    const promise = new Promise<PostDetails>((resolve, reject) => {

        https.get(`${redditUrl}/${filter}.json?limit=100`, (res) => {

            let text = '';

            res.on('data', (data) => {
                text += data.toString();
            });

            res.on('end', () => {

                const json = JSON.parse(text);
                const redditPage = <RedditPage>json;
                const childrenSize = redditPage.data.children.length;
                let postNum;
                const requestedPostNum = params[3];

                if (requestedPostNum && requestedPostNum.match(/^\d+$/) && parseInt(requestedPostNum) < childrenSize) postNum = parseInt(params[4]);

                else postNum = Math.floor(Math.random() * childrenSize);

                log.debug('getRedditPost', `children size : ${childrenSize}, random number: ${postNum}`);

                const {children} = redditPage.data;
                const post = children[postNum].data;

                log.debug('getRedditPost', `reddit post retrieved: ${JSON.stringify(post, null, 2)}`);

                resolve(post);

            });

            res.on('error', (e) => {

                log.debug('getRedditPost', `${e.toString()}`);

                reject();

            })
        });

    });

    promise.then(post => {

        let result;
        let options = null;
        let text;

        if (post.selftext && post.selftext !== '') {

            log.debug('getRedditPost', 'selftext found');

            if (post.selftext.length > 1000) text = post.selftext.substr(0, 1000);
            else text = post.selftext;

            result = `*${post.title}*\n\n${text}\n\n [link here](https://reddit.com${post.permalink})`;
            options = {disable_preview: true};
        }
        else if (post.selftext_html && post.selftext_html !== '') {

            log.debug('getRedditPost', 'selftext_html found');

            if (post.selftext_html.length > 1000) text = post.selftext_html.substr(0, 1000);
            else text = post.selftext_html;

            result = `*${post.title}*\n\n${text}\n\n [link here](https://reddit.com${post.permalink})`;
            options = {disable_preview: true};
        }
        else if (post.url && post.url !== '') {

            log.debug('getRedditPost', 'url found');

            result = `*${post.title}*\n\n${encodeURI(post.url)}`;
        }
        else {

            result = 'hmmm nothing found';
        }

        if (post.over_18) {

            log.debug('getRedditPost', `${post} is Nsfw`);

            result += '\nNSFW NSFW NSFW';
            options = {disable_preview: true};

        }
        else {

            log.debug('getRedditPost', `${subreddit} is sfw`);
        }

        if (post.gilded > 0) {
            result = `*You found a gilded post! Gold: ${post.gilded}*\n` + result;
        }

        sendTelegramResponse(new TelegramResponse(result, `${telegramRequest.chat.id}`), options);

    }).catch(() => {
        sendTelegramResponse(new TelegramResponse('sum ting wong', `${telegramRequest.chat.id}`));
    });
}

interface RedditPage {
    kind: string;
    data: {
        children: RedditPost[];
    }
}

interface RedditPost {
    kind: string;
    data: PostDetails;
}

interface PostDetails {
    id: string;
    name: string;
    url: string;
    post_hint: string;
    over_18: boolean;
    title: string;
    selftext: string;
    selftext_html: string;
    gilded: number;
    permalink: string;
}
