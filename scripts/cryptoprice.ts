import {sendTelegramResponse, TelegramRequest, TelegramResponse} from "./telegram";
import * as https from "https";
import {Logger} from "./logger";

const log = new Logger(__filename);

const symbols = ['BTC', 'DOGE', 'BCH', 'LTC', 'ETC', 'ETH'];

export function getPrice(telegramRequest: TelegramRequest) {

    const msg = telegramRequest.text;
    const symbol = msg.substr(1).trim().toUpperCase();

    if (symbols.indexOf(symbol) === -1) {
        sendResponse(telegramRequest, 'Unsupported symbol. Only LTC, BTC, BCH, and DOGE');
        return;
    }

    const baseUrl = `https://min-api.cryptocompare.com/data/price?fsym=${symbol}&tsyms=USD`;

    https.get(baseUrl, res => {

        let data = '';

        res.on('data', chunk => data += chunk.toString());

        res.on('end', () => {

            const json = JSON.parse(data);
            const result = `${symbol} : $ ${json.USD}`;
            sendResponse(telegramRequest, result);
        });

        res.on('error', e => {

            log.error('getPrice', e.toString());
            sendResponse(telegramRequest, 'Sum ting wong');
        });

    }).on('error', (e) => {

        log.error('getPrice', e.toString());
        sendResponse(telegramRequest, 'Sum ting wong');
        return;

    });
}

function sendResponse(telegramRequest: TelegramRequest, text: string) {

    sendTelegramResponse(new TelegramResponse(text, telegramRequest.chat.id));

}
