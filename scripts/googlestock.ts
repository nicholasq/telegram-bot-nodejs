/**
 * Created by nicholas on 4/8/17.
 */
import https = require('https');
import cheerio = require("cheerio");
import {Logger} from "./logger";

const log = new Logger(__filename);

export class GoogleStock {

    name: string;
    exchange: string;
    price: string;
    date: string;
    change: string;

    constructor(obj: any) {
        this.name = obj['t'];
        this.exchange = obj['e'];
        this.price = obj['l_cur'];
        this.date = obj['lt'];
        this.change = obj['cp_fix'];
    }

}

export function fetchGoogleStock(symbol: string): Promise<string> {
    const url = `https://finance.google.com/finance/info?client=ig&q=${symbol}`;

    return new Promise<string>((resolve, reject) => {
        let data = '';
        https.get(url, (res) => {
            res.on('data', (chunk) => {
                data += chunk;
            });
            res.on('end', () => {
                resolve(data);
            });
            res.on('error', () => {
                reject("");
            });
        }).on('error', () => {
            reject("");
        });
    });
}

export function parseGoogleResponse(response: string): JSON {
    if (response.match('httpserver\.cc') || !response.match('\\[')) {
        return null;
    }
    else {
        const json = JSON.parse(response.substr(3));
        return json[0];
    }
}

export function fetchAndParseGoogleStock(symbol: string): Promise<GoogleStock> {
    const stockPromise = fetchGoogleStock(symbol);
    return stockPromise.then((data) => {
            const parsed = parseGoogleResponse(data);
            if (parsed === null) {
                return null;
            }
            else {
                return new GoogleStock(parsed);
            }

        },
        () => {
            return null;
        });
}

export function fetchStockNews(googleStock: GoogleStock): Promise<string> {

    const url = `https://www.google.com/finance/company_news?q=${googleStock.exchange}%3A${googleStock.name}`;
    const html = new Promise<string>((resolve, reject) => {
        let html = '';

        https.get(url, res => {

            res.on('data', chunk => {
                html += chunk;
            });
            res.on('end', () => {
                resolve(html);
            });
            res.on('error', err => {

                log.debug('fetchStockNews', `${err.toString()}`);

                reject('');
            });
        }).on('error', err => {

            log.debug('fetchStockNews', err.toString());

            reject('');
        });
    });

    return html.then(html => {

        let $ = cheerio.load(html);
        const newsDiv = $('#news-main div').find('a');
        return newsDiv.get(0).children[0].data + '\n' +
            newsDiv.get(1).children[0].data + '\n' +
            newsDiv.get(2).children[0].data

    }).catch(() => {
        return '';
    });
}
