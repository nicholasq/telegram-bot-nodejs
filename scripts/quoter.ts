import {sendTelegramResponse, TelegramRequest, TelegramResponse} from "./telegram";
import {isUndefined} from "util";
import {Logger} from "./logger";

const quotes = {};
const log = new Logger(__filename);

export function setQuote(telegramRequest: TelegramRequest) {

    log.debug('setQuote', `${JSON.stringify(telegramRequest)}`);
    const text = telegramRequest.text
        .replace(/\s+/g, ' ')
        .replace(/\/setquote/, '');

    if (text.length === 0) {
        sendTelegramResponse(new TelegramResponse("Type a quote fool", `${telegramRequest.chat.id}`));
        return;
    }
    else if (text.indexOf('-') === -1) {
        sendTelegramResponse(new TelegramResponse("Specify your name after like this: - name", `${telegramRequest.chat.id}`));
        return;
    }

    const [quote, name] = text.split('-');

    log.debug('setQuote', quote);
    quotes[name.trim()] = quote;
    log.debug('setQuote', `${JSON.stringify(quotes)}`);

    sendTelegramResponse(new TelegramResponse("Done", `${telegramRequest.chat.id}`));
}

export function quoteMe(telegramRequest: TelegramRequest) {

    log.debug('quoteMe', `${JSON.stringify(telegramRequest)}`);
    const text = telegramRequest.text.replace(/\s+/g, ' ').split(' ');

    if (text.length < 2) {
        sendTelegramResponse(new TelegramResponse("I need a name", `${telegramRequest.chat.id}`));
        return;
    }

    const name = text.slice(1).join(' ').trim();
    const quote = quotes[name];

    if (quote == null || isUndefined(quote)) {
        sendTelegramResponse(new TelegramResponse(`Couldn't find a name for ${name}. Case sensitivity matters.`, `${telegramRequest.chat.id}`));
    }
    else {
        sendTelegramResponse(new TelegramResponse(`${quote} \n \t- ${name}`, `${telegramRequest.chat.id}`));
    }
}