/**
 * Created by nicholas on 4/26/17.
 */

import {executeCommand} from "./commandExecutor";
import * as fs from "fs";
import {sendTelegramResponse, TelegramRequest, TelegramResponse} from "./telegram";
import {Logger} from "./logger";

const log = new Logger(__filename);

export function executeKotlin(code: string): Promise<string> {

    const className = `Temp${Date.now()}`;
    const fileName = `${className}.kt`;
    const compiledName = `${className}.class`;

    const fileWrite = new Promise((resolve1, reject1) => {

        fs.writeFile(fileName, buildKotlinProgram(className, code),

            (err) => {

                if (err) {
                    reject1("problem writing kotlin files");
                }
                else {
                    resolve1("file write successful");
                }
            }
        );
    });

    const compiled = new Promise((resolve, reject) => {

        fileWrite.then(success => {

            log.debug('executeKotlin', `${success}`);

            log.debug('executeKotlin', 'attempting to compile kotlin');

            const executed = executeCommand('kotlinc', [fileName]);

            executed.then((result) => {
                resolve(result);
            }).catch((err) => {
                reject(err)
            });
        }).catch((err) => {

            log.error('executeKotlin', `${err}`);

            reject("could not compile kotlin");
        });
    });

    return new Promise<string>((resolve, reject) => {

        compiled.then(() => {

            log.debug('executeKotlin', 'attempt to execute kotlin program');

            const executed = executeCommand('kotlin', [className]);

            executed.then(executedMsg => {

                log.debug('executeKotlin', executedMsg);

                cleanup(fileName, compiledName);

                log.debug('executeKotlin', 'kotlin program executed successfully');

                resolve(executedMsg);
            });

            executed.catch((err) => {

                log.error('executeKotlin', err);

                log.error('executeKotlin', 'could not execute kotlin program');

                cleanup(fileName, compiledName);
                reject(err);
            });
        }).catch(err => {

            log.error('executeKotlin', 'failed trying to compile kotlin');

            log.error('executeKotlin', `${err}`);

            cleanup(fileName, compiledName);
            reject(`${err}`);
        });

    });
}

function cleanup(source: string, compiled: string) {

    try {

        fs.access(source, fs.constants.F_OK, err => {

            if (!err) {
                fs.unlink(source);
            }
            else {

                log.error('cleanup', `${err}`);
            }
        });

        fs.access(compiled, fs.constants.F_OK, err => {

            if (!err) {
                fs.unlink(compiled);
            }
            else {

                log.error('cleanup', `${err}`);
            }
        });
    } catch (err) {

        log.error('cleanup', `${err}`);
    }
}

function buildKotlinProgram(className: string, code: string) {

    return `
           import java.util.*; 
           import java.util.function.*; 
           class ${className} {
               public static void main(String[] args) {
               ${code}
               }
           }`
}

export function handleKotlinRequest(request: TelegramRequest) {

    const kotlinProgram = request.text.substr(5).trim();
    const executed = executeKotlin(kotlinProgram);

    executed.then(result => {

        log.debug('handleKotlinRequest', 'we got back a result from kotlin execution');

        log.debug('handleKotlinRequest', `${result}`);

        sendTelegramResponse(new TelegramResponse(`${result}`, request.chat.id));
    });
    executed.catch(error => {

        log.error('handleKotlinRequest', `${error}`);

        sendTelegramResponse(new TelegramResponse(`${error}`, request.chat.id));
    });
}

