import {sendTelegramResponse, TelegramRequest, TelegramResponse} from "./telegram";
import {Logger} from "./logger";
import https = require("https");
import cheerio = require("cheerio");

const log = new Logger(__filename);

export function trendingFinviz(telgramRequest: TelegramRequest) {

    https.get('https://finviz.com', res => {

        let page;

        log.debug('trendingFinviz', 'begin download');

        res.on('data', d => page += d.toString())

            .on('error', e => {

                log.error('trendingFinviz', `${e}`);

            })
            .on('end', () => {

                const stocks = [];

                let $ = cheerio.load(page);
                const newsDiv = $('.t-home-table');
                const firstTable = newsDiv.first();

                firstTable.find('tr').next().each(function (num) {
                    stocks.push([]);
                    $(this).find('td').each(function () {

                        stocks[num].push($(this).text());
                    })
                });

                log.info('trendingFinviz', JSON.stringify(stocks, null, 2));

                const msg = stocks.map(item => {
                    return `*Ticker:*    ${item[0]}\n*Last:*        ${item[1]}\n*Change:*  ${item[2]}\n*Volume:*  ${item[3]}\n*Signal:*     ${item[5]}\n`;
                }).join('-------------------------\n');

                log.info('trendingFinviz', msg);

                sendTelegramResponse(new TelegramResponse(msg, `${telgramRequest.chat.id}`));

            });
    })
}

export function marketFinviz(telegramRequest: TelegramRequest) {

    getMarketGraphs((urls: string[]) => {
        submitMarketResponses(urls, telegramRequest)
    });
}

export function getMarketGraphs(callback: ((arg: string[]) => void)) {

    https.get('https://finviz.com', res => {

        let page = '';

        res.on('data', data => page += data.toString());
        res.on('error', e => page = 'error');
        res.on('end', () => {

            const imageLinks = [];
            let $ = cheerio.load(page);
            const tables = $('table');
            const marketTables = tables.next().next().find('table').find('table').find('table');
            const images = marketTables.find('tr').find('img');

            images.each(function (index, element) {

                const src = $(element).attr('src');

                if (src.search('.*dow&rev.*') === 0 ||
                    src.search('.*nasdaq&rev.*') === 0 ||
                    src.search('.*sp500&rev.*') === 0) {

                    imageLinks.push(src);
                }
            });

            callback(
                imageLinks
                    .map(it => `https://finviz.com/${it}`)
            )

        });
    });

}

function submitMarketResponses(urls: string[], telegramRequest: TelegramRequest) {

    urls.forEach(link => {

        setTimeout(function () {

            sendTelegramResponse(new TelegramResponse(link, `${telegramRequest.chat.id}`));

        }, 1000);
    });
}
