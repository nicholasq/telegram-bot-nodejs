import {log} from "util";

export class Logger {
    fname: string;

    public constructor(filename: string) {
        this.fname = filename;
    }

    debug(func: string, msg: string) {
        log(`[DEBUG] ${func}() - ${this.fname} - ${msg}`);
    }

    info(func: string, msg: string) {
        log(`[INFO] ${func}() - ${this.fname} - ${msg}`);
    }

    warn(func: string, msg: string) {
        log(`[WARN] ${func}() - ${this.fname} - ${msg}`);
    }

    error(func: string, msg: string) {
        log(`[ERROR] ${func}() - ${this.fname} - ${msg}`);
    }
}

export function logger(fname: string) {
    return new Logger(fname);
}