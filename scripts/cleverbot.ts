import {sendTelegramResponse, TelegramRequest, TelegramResponse} from "./telegram";
import * as fs from "fs";
import * as https from "https";
import {logger} from "./logger";

const apiKey = fs.readFileSync(`${__dirname}/../../resource/cleverbotapikey`).toString().trim();
let conversationId;
const log = logger(__filename);

export function handleCleverBot(telegramRequest: TelegramRequest) {

    const userMsg = telegramRequest.text.trim().replace(/[jJ]arvis/g, ' ');
    let url = `https://www.cleverbot.com/getreply?key=${apiKey}&input=${userMsg}`;
    if (conversationId && conversationId !== '') url += `&cs=${conversationId}`;

    log.debug('handleCleverBot', `${url}`);

    const botReply = new Promise<string>((resolve, reject) => {

        https.get(url, res => {

            let data = '';

            res.on('data', chunk => data += chunk.toString());
            res.on('end', () => resolve(data));
            res.on('error', e => reject(e.toString()));

        }).on('error', (e) => {

            log.debug('handleCleverBot', e.toString());

            reject(e.toString());
        });
    });

    botReply.then(data => {

        log.debug("handleCleverBot", `returned json: ${data}`);

        const botReply = <BotReply>JSON.parse(data);

        log.debug('handleCleverBot', `${JSON.stringify(botReply, null, 2)}`);

        let msg;

        if (botReply.output && botReply.output !== '') {
            msg = botReply.output;
        }
        else {
            msg = 'hmmm idk';
        }

        if (botReply.cs !== '') conversationId = botReply.cs;

        log.debug('handleCleverBot', msg);

        sendTelegramResponse(new TelegramResponse(msg, telegramRequest.chat.id));

        if (botReply.interaction_count && botReply.interaction_count % 1000 === 0) {
            sendTelegramResponse(new TelegramResponse(`${botReply.interaction_count}/5000`, telegramRequest.chat.id));
        }

    });
    botReply.catch(e => {

        log.debug('handleCleverBot', e);
    });
}

interface BotReply {
    cs: string;
    interaction_count: number;
    input: string;
    output: string;
    conversation_id: string;
    errorline: string;
}
