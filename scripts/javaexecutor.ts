/**
 * Created by nicholas on 4/26/17.
 */

import {executeCommand} from "./commandExecutor";
import * as fs from "fs";
import {sendTelegramResponse, TelegramRequest, TelegramResponse} from "./telegram";
import {Logger} from "./logger";

const log = new Logger(__filename);

export function executeJava(code: string): Promise<string> {

    const className = `Temp${Date.now()}`;
    const fileName = `${className}.java`;
    const compiledName = `${className}.class`;

    const fileWrite = new Promise((resolve1, reject1) => {

        fs.writeFile(fileName, buildJavaProgram(className, code),

            (err) => {

                if (err) {
                    reject1("problem writing java files");
                }
                else {
                    resolve1("file write successful");
                }
            }
        );
    });

    const compiled = new Promise((resolve, reject) => {

        fileWrite.then(success => {

            log.debug('executeJava', `${success}`);

            log.debug('executeJava', 'attempting to compile java');

            const executed = executeCommand('javac', [fileName]);

            executed.then((result) => {
                resolve(result);
            }).catch((err) => {
                reject(err)
            });
        }).catch((err) => {

            log.error('executeJava', `${err}`);

            reject("could not compile java");
        });
    });

    return new Promise<string>((resolve, reject) => {

        compiled.then(() => {

            log.debug('executeJava', 'attempt to execute java program');

            const executed = executeCommand('java', [className]);

            executed.then(executedMsg => {

                log.debug('executeJava', executedMsg);

                cleanup(fileName, compiledName);

                log.debug('executeJava', 'java program executed successfully');

                resolve(executedMsg);
            });

            executed.catch((err) => {

                log.error('executeJava', err);

                log.error('executeJava', 'could not execute java program');

                cleanup(fileName, compiledName);
                reject(err);
            });
        }).catch(err => {

            log.error('executeJava', 'failed trying to compile java');

            log.error('executeJava', `${err}`);

            cleanup(fileName, compiledName);
            reject(`${err}`);
        });

    });
}

function cleanup(source: string, compiled: string) {

    try {

        fs.access(source, fs.constants.F_OK, err => {

            if (!err) {
                fs.unlink(source);
            }
            else {

                log.error('cleanup', `${err}`);
            }
        });

        fs.access(compiled, fs.constants.F_OK, err => {

            if (!err) {
                fs.unlink(compiled);
            }
            else {

                log.error('cleanup', `${err}`);
            }
        });
    } catch (err) {

        log.error('cleanup', `${err}`);
    }
}

function buildJavaProgram(className: string, code: string) {

    return `
           import java.util.*; 
           import java.util.function.*; 
           class ${className} {
               public static void main(String[] args) {
               ${code}
               }
           }`
}

export function handleJavaRequest(request: TelegramRequest) {

    const javaProgram = request.text.substr(5).trim();
    const executed = executeJava(javaProgram);

    executed.then(result => {

        log.debug('handleJavaRequest', 'we got back a result from java execution');

        log.debug('handleJavaRequest', `${result}`);

        sendTelegramResponse(new TelegramResponse(`${result}`, request.chat.id));
    });
    executed.catch(error => {

        log.error('handleJavaRequest', `${error}`);

        sendTelegramResponse(new TelegramResponse(`${error}`, request.chat.id));
    });
}

