import * as https from "https";
import * as fs from "fs";
import {Logger} from "./logger";

/**
 * Created by nicholas on 4/9/17.
 */

const telegramKey = getTelegramApiKey().trim();
const baseUrl = `https://api.telegram.org/bot${telegramKey}`;
const urlPath = `/bot${telegramKey}`;
const log = new Logger(__filename);

export class TelegramResponse {
    constructor(public text: string, public chat_id: string | number) {
    }
}

export interface Chat {
    id: number;
    type: string;
    title: string;
    username: string;
    first_name: string;
    last_name: string;
    all_members_are_administrators: boolean;
}

export interface TelegramRequest {
    message_id: number;
    from: Person;
    date: number;
    chat: Chat;
    text: string;
    photo: any[];
    document: any;
}

export interface Person {
    id: number;
    first_name: string;
    last_name: string;
}

export interface SendMessage {
    chat_id: number;
    text: string;
    parse_mode: string;
    disable_web_page_preview: boolean;
    disable_notification: boolean;
    reply_to_message_id: number;
    reply_markup: any;
}

export function getTelegramApiKey(): string {

    return fs.readFileSync(`${__dirname}/../../resource/apikey`, {encoding: 'utf8'});
}

export function sendTelegramResponse(msg: TelegramResponse, options?: any) {
    sendTelegramResponsePost(msg, options);
}

export function sendTelegramResponseGet(msg: TelegramResponse, options?: any) {

    let url = `${baseUrl}/sendMessage?` +
        `chat_id=${msg.chat_id}&` +
        `text=${msg.text}&` +
        `parse_mode=Markdown&` +
        `disable_notification=true`;
    if (options) {
        if (options.disable_preview) {
            url += '&disable_web_page_preview=true';
        }
    }

    sendToTelegramGet(url);
}

export function sendTelegramResponsePost(msg: TelegramResponse, options?: any) {

    const sendMessage = <SendMessage>msg;

    if (options && options.disable_preview) {
        sendMessage.disable_web_page_preview = true;
    }

    sendMessage.parse_mode = 'Markdown';
    sendMessage.disable_notification = true;

    const stringified = JSON.stringify(sendMessage, null, 2);

    log.debug('sendTelegramResponsePost', `${stringified}`);

    const request = {
        hostname: 'api.telegram.org',
        post: 443,
        path: `${urlPath}/sendMessage`,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': Buffer.byteLength(stringified)
        }
    };

    const post = https.request(request, res => {

        log.debug('sendTelegramResponsePost', `status: ${res.statusCode}`);

        log.debug('sendTelegramResponsePost', `headers: ${JSON.stringify(res.headers, null, 2)}`);

        let data = '';

        res
            .on('data', chunk => data += `${chunk}`)
            .on('end', () => log.debug('sendTelegramResponsePost', `${data}`))
    });

    post.on('error', e => {
        log.error('sendTelegramResponsePost', `${e}`);
    });

    post.write(stringified);
    post.end();
}

export function deleteMessage(telegramRequest: TelegramRequest) {

    const chatId = telegramRequest.chat.id;
    const msgId = telegramRequest.message_id;
    let url = `${baseUrl}/deleteMessage?chat_id=${chatId}&message_id=${msgId}`;

    log.debug('deleteMessage', `${url}`);

    sendToTelegramGet(url);
}

function sendToTelegramGet(url: string) {

    const encodedUri = encodeURI(url);

    log.debug('sendToTelegramGet', `sending to telegram: ${encodedUri}`);

    https.get(encodedUri, res => {

        log.debug('sendToTelegramGet', `status code ${res.statusCode}`);

        res.on('data', d => {

            log.debug('sendToTelegramGet', `${d}`);

        });

    }).on('error', e => {

        log.debug('sendToTelegramGet', `${e}`);

    });
}
