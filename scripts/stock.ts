import {sendTelegramResponse, TelegramRequest, TelegramResponse} from "./telegram";
import {fetchAndParseGoogleStock, fetchStockNews, GoogleStock} from "./googlestock";
import * as os from "os";
import {fetchStockDataForAll, fetchTrendingStockTwits} from "./stockTwits";
import {Logger} from "./logger";

const log = new Logger(__filename);

export function handleStockRequest(request: TelegramRequest) {

    if (request.text.startsWith("/")) {

        const stock = request.text.substr(1).trim();
        const stockPromise = fetchAndParseGoogleStock(stock);

        log.debug('handleStockRequest', `handling stock request for ${stock}`);

        stockPromise.then((stock) => {

            if (stock === null) {

                sendTelegramResponse(new TelegramResponse('sum ting wong',
                    `${request.chat.id}`
                ));
            }
            else {
                const newsPromise = fetchStockNews(stock);
                newsPromise.then(news => {
                    const formatted = formatStock(stock, news);
                    sendTelegramResponse(new TelegramResponse(formatted,
                        `${request.chat.id}`
                    ));

                }).catch(error => {

                    log.debug('handleStockRequest', `${error}`);

                    const formatted = formatStock(stock, 'no news found');

                    sendTelegramResponse(new TelegramResponse(formatted,
                        `${request.chat.id}`
                    ));
                });
            }
        });
        stockPromise.catch((err) => {

            log.debug('handleStockRequest', `${err}`);

            sendTelegramResponse(new TelegramResponse('Stock not found.',

                `${request.chat.id}`
            ));
        });
    }
}

export function handleStocktwits(request: TelegramRequest) {

    const trending = fetchTrendingStockTwits();
    const params = request.text.trim().replace(/\s+/g, ' ').split(' ');

    trending.then((stocks) => {

        const promise = fetchStockDataForAll(stocks);
        promise.then((googleStocks) => {

            const formatted = googleStocks
                .sort((a, b) => {

                    if (params.length > 1) {

                        if (params[1].toLowerCase() === 'name') {

                            return a.name.localeCompare(b.name);
                        }
                        else if (params[1].toLowerCase() === 'price') {

                            const numA = Number.parseFloat(a.price);
                            const numB = Number.parseFloat(b.price);

                            if (numA > numB) {
                                return 1;
                            }
                            else if (numA < numB) {
                                return -1;
                            }
                            else {
                                return 0;
                            }
                        }
                        else if (params[1].toLowerCase() === 'change') {

                            const numA = Number.parseFloat(a.change);
                            const numB = Number.parseFloat(b.change);

                            if (numA > numB) {
                                return 1;
                            }
                            else if (numA < numB) {
                                return -1;
                            }
                            else {
                                return 0;
                            }
                        }
                        else {
                            return 0;
                        }
                    }
                    else {
                        return 0;
                    }
                })
                .map((value) => {

                    let name = (value.name + '      ').substr(0, 6);
                    let change = (value.change + '       ').substr(0, 7);
                    let price = (value.price + '        ').substr(0, 8);

                    if (Number.parseFloat(value.price) > 4.9 || Number.parseFloat(value.price) < -4.9) {

                        return `${name} : *${change}%* : ${price}`;
                    }
                    else {

                        return `${name} : ${change}% : ${price}`;
                    }
                })
                .join("\n");

            sendTelegramResponse(new TelegramResponse(formatted, `${request.chat.id}`));
        });
        promise.catch((err) => {

            log.debug('handleUpdateRequest', `${err}`);
        });
    });

    trending.catch((err) => {

        log.debug('handleUpdateRequest', `${err}`);

        sendTelegramResponse(new TelegramResponse('Couldn\'t do update.', `${request.chat.id}`));
    });
}

function formatStock(stock: GoogleStock, news: string) {

    const charturl = `https://www.google.com/finance/getchart?q=${stock.name}&x=${stock.exchange}&p=5d&i=240&ei=xyjfWPmuDYKJ2AapuJCIAg`;

    return [
        `*${stock.name.toUpperCase()}*`,
        ``,
        `${stock.exchange.toUpperCase()}`,
        `*price*  : ${stock.price}`,
        `_change_  : ${stock.change}%`,
        `*date*   : ${stock.date}`,
        ``,
        `*NEWS* --`,
        `${news}`,
        ``,
        charturl].join(os.EOL);
}
