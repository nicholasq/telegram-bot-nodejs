import {getTelegramApiKey} from "./telegram";
/**
 * Created by dpnicholas on 3/3/2017.
 */

export const apiKey = `${getTelegramApiKey().trim()}`;
export const nixqloud = 'https://nixqloud.xyz';
export const telegram = 'https://api.telegram.org';
export const telegramBotUrl = `${telegram}/bot${apiKey}`;
export const deregisterBotUrl = `${telegramBotUrl}/deleteWebhook`;
export const registerBotUrl = `${telegramBotUrl}/setWebhook?url=${nixqloud}/telegram`;
export const getBotInfo = `${telegramBotUrl}/getMe`;
