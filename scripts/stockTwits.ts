/**
 * Created by nicholas on 4/8/17.
 */

import https = require('https');
import cheerio = require("cheerio");
import {fetchAndParseGoogleStock, GoogleStock} from "./googlestock";

function getHtml(): Promise<string> {

    const url = 'https://stocktwits.com';

    return new Promise<string>((resolve, reject) => {

        https.get(url, (res) => {

            let stuff = '';

            res.on('data', (chunk) => {
                stuff += chunk;
            });
            res.on('end', () => {
                resolve(stuff);
            });
            res.on('error', (err) => {
                reject(err);
            });
        });
    });

}
export function fetchTrendingStockTwits(): Promise<string[]> {

    const html = getHtml;

    return html().then((data) => {

        const symbols = Array<string>();
        const $ = cheerio.load(data);

        $('#scrollingText').find('a').each((i, elem) => {
            let symbol = $(elem).text().trim();
            symbols.push(symbol);
        });

        return symbols;
    });
}

export function fetchStockDataForAll(stocks: string[]): Promise<GoogleStock[]> {

    const promises = stocks.map((value) => {

        return fetchAndParseGoogleStock(value);
    });
    // return a promise that waits on all of the promises to finish.
    return Promise.all(promises);
}

