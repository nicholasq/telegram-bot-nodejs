import {executeCommand} from "./commandExecutor";
import {sendTelegramResponse, TelegramRequest, TelegramResponse} from "./telegram";
import {Logger} from "./logger";

/**
 * Created by nicholas on 4/26/17.
 */

const log = new Logger(__filename);

export function handleJavascriptRequest(request: TelegramRequest) {

    const javascriptCommand = request.text.substr('/javascript'.length).trim();
    const executed = executeJavascript(javascriptCommand);

    executed.then(result => {
        sendTelegramResponse(new TelegramResponse(result, `${request.chat.id}`));
    });

    executed.catch(error => {

        log.error('handleJavascriptRequest', `${error}`);

        sendTelegramResponse(new TelegramResponse(`${error}`, `${request.chat.id}`));
    });
}

function executeJavascript(command: string): Promise<string> {

    return new Promise((resolve, reject) => {

        if (command.indexOf("open(") != -1) {
            resolve('nuh uh. you ain\'t gettin no file access!');
        }
        else {

            const javascript = executeCommand('node', ['-e', command]);
            javascript.then(result => resolve(result));
            javascript.catch(err => reject(err));
        }
    });
}
