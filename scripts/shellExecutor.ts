import {executeCommand} from "./commandExecutor";
import {sendTelegramResponse, TelegramRequest, TelegramResponse} from "./telegram";
import {Logger} from "./logger";

const log = new Logger(__filename);

export function executeShell(request: TelegramRequest) {

    if (request.from.first_name.trim() !== 'Nicholas') {

        sendTelegramResponse(new TelegramResponse('nuh uh. only massa can get a shell', `${request.chat.id}`));
        return;
    }

    const shellcode = request.text.substr('/shell'.length).trim();
    const executed = executeCommand("bash", ['-c', shellcode]);

    executed.then(result => {

        const output = result.substr(0, 1000);

        log.debug('executeShell', output);

        sendTelegramResponse(new TelegramResponse(output, `${request.chat.id}`));
    });

    executed.catch(error => {

        log.error('executeShell', `${error}`);

        sendTelegramResponse(new TelegramResponse(`${error}`, `${request.chat.id}`));
    });
}
