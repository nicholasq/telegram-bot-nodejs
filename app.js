const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const telegram = require('./compiled/routes/message');
const status = require('./compiled/routes/status');

const log = require('util').log;

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
// app.use(require('node-sass-middleware')({
//     src: path.join(__dirname, 'public'),
//     dest: path.join(__dirname, 'public'),
//     indentedSyntax: true,
//     sourceMap: true
// }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/telegram', telegram);
app.use('/status', status);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

const https = require('https');
const bot = require('./compiled/scripts/bot');

const initBotPromise = new Promise((resolve, reject) => {
    log("deregistering bot");
    https.get(bot.deregisterBotUrl, (res) => {
        res.on('data', (data) => {
            resolve(`${data}`);
        });
    }).on('error', (e) => {
        reject(`${e}`);
    });
});

initBotPromise.then(result => {
    log(`${result}`);
    log("registering bot");
    https.get(bot.registerBotUrl, (res) => {
        res.on('data', (data) => {
            log(`response: ${data}`);
        });
    }).on('error', (e) => {
        log(`couldn\'t register bot: ${e}`);
    });
});
initBotPromise.catch(error => {
    log("something went wrong trying to register bot");
    log(`${error}`);
});


log('getting info for bot');
https.get(bot.getBotInfo, res => {

    let json = '';

    res.on('data', data => {
        json += `${data}`;
    });

    res.on('end', () => {

        log('bot data:');
        log(JSON.stringify(json, null, 2));
    });

}).on('error', e => {
    log('couldn\'t get info on bot');
    log(e);
});


module.exports = app;
